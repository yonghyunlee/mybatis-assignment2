package io.wisoft.seminar.drama;

import java.util.StringJoiner;

public class Drama {

  private String code;
  private String name;
  private String provider;
  private String broad;
  private String openDate;

  public Drama() {
  }

  public Drama(String code, String name, String provider, String broad, String openDate) {
    this.code = code;
    this.name = name;
    this.provider = provider;
    this.broad = broad;
    this.openDate = openDate;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Drama.class.getSimpleName() + "[", "]")
        .add("code='" + code + "'")
        .add("name='" + name + "'")
        .add("provider='" + provider + "'")
        .add("broad='" + broad + "'")
        .add("openDate='" + openDate + "'")
        .toString();
  }
}
