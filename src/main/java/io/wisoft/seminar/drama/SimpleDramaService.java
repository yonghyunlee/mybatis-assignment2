package io.wisoft.seminar.drama;

import io.wisoft.seminar.configure.ConnectionMaker;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class SimpleDramaService implements DramaService {

  private ConnectionMaker connectionMaker = new ConnectionMaker();

  @Override
  public List<Drama> getDramaOpDateIsNull() {
    List<Drama> dramaList = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      DramaService dramaService = sqlSession.getMapper(DramaService.class);
      dramaList = dramaService.getDramaOpDateIsNull();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return dramaList;
  }

  @Override
  public List<Drama> getDramaProvider() {
    List<Drama> dramaList = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      DramaService dramaService = sqlSession.getMapper(DramaService.class);
      dramaList = dramaService.getDramaProvider();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return dramaList;
  }

  @Override
  public int updateDramaOpDateNull() {
    int result = 0;

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      DramaService dramaService = sqlSession.getMapper(DramaService.class);
      result = dramaService.updateDramaOpDateNull();

      sqlSession.commit();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public List<Drama> getDramaByBroad(List<String> broad) {
    List<Drama> dramaList = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      DramaService dramaService = sqlSession.getMapper(DramaService.class);
      dramaList = dramaService.getDramaByBroad(broad);
    } catch (Exception e) {
      e.printStackTrace();
    }

    return dramaList;
  }

  @Override
  public List<Drama> getDramaCodeAndName() {
    List<Drama> dramaList = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      DramaService dramaService = sqlSession.getMapper(DramaService.class);
      dramaList = dramaService.getDramaCodeAndName();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return dramaList;
  }
}
