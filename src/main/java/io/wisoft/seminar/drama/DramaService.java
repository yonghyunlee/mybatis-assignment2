package io.wisoft.seminar.drama;

import java.util.List;

public interface DramaService {

  List<Drama> getDramaCodeAndName();

  List<Drama> getDramaByBroad(List<String> broad);

  List<Drama> getDramaProvider();

  List<Drama> getDramaOpDateIsNull();

  int updateDramaOpDateNull();

}
