package io.wisoft.seminar;

import io.wisoft.seminar.department.Department;
import io.wisoft.seminar.department.SimpleDepartmentService;
import io.wisoft.seminar.department.DepartmentService;
import io.wisoft.seminar.drama.Drama;
import io.wisoft.seminar.drama.DramaService;
import io.wisoft.seminar.drama.SimpleDramaService;
import io.wisoft.seminar.employee.*;
import org.apache.commons.lang3.time.StopWatch;

import java.util.Arrays;
import java.util.List;

public class Application {

  public static void main(String[] args) {

    System.out.println("Hello, App!");
    final StopWatch stopWatch = new StopWatch();


    System.out.println("question 1");
    final DepartmentService departmentService = new SimpleDepartmentService();
    List<Department> departmentList = departmentService.getDepartmentAll();
    departmentList.forEach(System.out::println);
    System.out.println();


    System.out.println("question 2");
    final EmployeeService employeeService = new SimpleEmployeeService();
    List<Employee> employeeList = employeeService.getEmployeeAll();
    employeeList.forEach(System.out::println);
    System.out.println();

    System.out.println("question 3");
    final DramaService dramaService = new SimpleDramaService();
    List<Drama> dramaList = dramaService.getDramaCodeAndName();
    dramaList.forEach(System.out::println);
    System.out.println();

    System.out.println("question 4");
    List<String> board = Arrays.asList("KBC", "SBC");
    dramaList = dramaService.getDramaByBroad(board);
    dramaList.forEach(System.out::println);
    System.out.println();

    System.out.println("question 5");
    dramaList = dramaService.getDramaProvider();
    dramaList.forEach(System.out::println);
    System.out.println();

    System.out.println("question 6");
    List<EmployeeSalary> result = employeeService.getSalaryCountAndAverage();
    result.forEach(System.out::println);
    System.out.println();

    System.out.println("question 7");
    dramaList = dramaService.getDramaOpDateIsNull();
    dramaList.forEach(System.out::println);
    System.out.println();

    System.out.println("question 8");
    List<EmployeeAndManager> employeeAndManager = employeeService.getEmployeeAndManager();
    employeeAndManager.forEach(System.out::println);
    System.out.println();

    System.out.println("question 9");
    employeeList = employeeService.getEmployeeNameAndSalary();
    employeeList.forEach(System.out::println);
    System.out.println();

    System.out.println("question 10");
    List<EmployeeGroupByRank> result10 = employeeService.getEmployeeGroupByRank();
    result10.forEach(System.out::println);
    System.out.println();

    System.out.println("question 11");
    List<Employee> result11 = employeeService.getEmployeeMoreAvg();
    result11.forEach(System.out::println);
    System.out.println();

    System.out.println("question 12");
    int result12 = dramaService.updateDramaOpDateNull();
    System.out.println(result12);

    System.out.println("question 13");
    int result13 = employeeService.updateRankAndSalary();
    System.out.println(result13);

    System.out.println("question 14");
    Employee employee = new Employee("E903", "손진현", "E901", 4000);
    int result14 = employeeService.insertExecutive(employee);
    System.out.println(result14);

    System.out.println("question 15");
    int result15 = employeeService.deleteEmployee(employee);
    System.out.println(result15);
  }
}
