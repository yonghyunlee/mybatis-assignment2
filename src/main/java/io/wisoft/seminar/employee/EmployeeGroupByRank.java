package io.wisoft.seminar.employee;

import java.util.StringJoiner;

public class EmployeeGroupByRank {

  private String rank;
  private float avg;
  private int min;
  private int max;

  public EmployeeGroupByRank() {
  }

  public EmployeeGroupByRank(String rank, float avg, int min, int max) {
    this.rank = rank;
    this.avg = avg;
    this.min = min;
    this.max = max;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", EmployeeGroupByRank.class.getSimpleName() + "[", "]")
        .add("rank='" + rank + "'")
        .add("avg=" + avg)
        .add("min=" + min)
        .add("max=" + max)
        .toString();
  }
}
