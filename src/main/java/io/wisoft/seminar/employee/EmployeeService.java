package io.wisoft.seminar.employee;

import java.util.List;

public interface EmployeeService {

  List<Employee> getEmployeeAll();

  List<EmployeeSalary> getSalaryCountAndAverage();

  List<EmployeeAndManager> getEmployeeAndManager();

  List<Employee> getEmployeeNameAndSalary();

  List<EmployeeGroupByRank> getEmployeeGroupByRank();

  List<Employee> getEmployeeMoreAvg();

  int updateRankAndSalary();

  int insertExecutive(Employee employee);

  int deleteEmployee(Employee employee);

}
