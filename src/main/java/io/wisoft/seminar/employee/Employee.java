package io.wisoft.seminar.employee;

import java.util.StringJoiner;

public class Employee {

  private String code;
  private String name;
  private String manager;
  private int salary;

  public Employee() {

  }

  public Employee(String code, String name, String manager, int salary) {
    this.code = code;
    this.name = name;
    this.manager = manager;
    this.salary = salary;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Employee.class.getSimpleName() + "[", "]")
        .add("code='" + code + "'")
        .add("name='" + name + "'")
        .add("manager='" + manager + "'")
        .add("salary=" + salary)
        .toString();
  }
}
