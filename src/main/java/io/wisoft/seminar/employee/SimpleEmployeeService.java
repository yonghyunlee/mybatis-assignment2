package io.wisoft.seminar.employee;

import io.wisoft.seminar.configure.ConnectionMaker;
import org.apache.ibatis.session.SqlSession;

import java.util.ArrayList;
import java.util.List;

public class SimpleEmployeeService implements EmployeeService {

  final ConnectionMaker connectionMaker = new ConnectionMaker();

  @Override
  public List<EmployeeGroupByRank> getEmployeeGroupByRank() {
    List<EmployeeGroupByRank> employees = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      employees = service.getEmployeeGroupByRank();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return employees;
  }

  @Override
  public int deleteEmployee(Employee employee) {
    int result = 0;

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      result = service.deleteEmployee(employee);

      sqlSession.commit();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public int insertExecutive(Employee employee) {
    int result = 0;

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      result = service.insertExecutive(employee);

      sqlSession.commit();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public int updateRankAndSalary() {
    int result = 0;

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      result = service.updateRankAndSalary();

      sqlSession.commit();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public List<Employee> getEmployeeMoreAvg() {
    List<Employee> employees = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      employees = service.getEmployeeMoreAvg();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return employees;
  }

  @Override
  public List<Employee> getEmployeeNameAndSalary() {
    List<Employee> employees = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      employees = service.getEmployeeNameAndSalary();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return employees;
  }

  @Override
  public List<EmployeeAndManager> getEmployeeAndManager() {
    List<EmployeeAndManager> result = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      result = service.getEmployeeAndManager();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public List<EmployeeSalary> getSalaryCountAndAverage() {
    List<EmployeeSalary> result = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      result = service.getSalaryCountAndAverage();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return result;
  }

  @Override
  public List<Employee> getEmployeeAll() {
    List<Employee> employees = new ArrayList<>();

    try (SqlSession sqlSession = connectionMaker.getSqlSession()) {
      final EmployeeService service = sqlSession.getMapper(EmployeeService.class);
      employees = service.getEmployeeAll();
    } catch (Exception e) {
      e.printStackTrace();
    }

    return employees;
  }

}
