package io.wisoft.seminar.employee;

import java.util.StringJoiner;

public class EmployeeAndManager {

  private String name;
  private String manager;

  public EmployeeAndManager(String name, String manager) {
    this.name = name;
    this.manager = manager;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", EmployeeAndManager.class.getSimpleName() + "[", "]")
        .add("name='" + name + "'")
        .add("manager='" + manager + "'")
        .toString();
  }
}
