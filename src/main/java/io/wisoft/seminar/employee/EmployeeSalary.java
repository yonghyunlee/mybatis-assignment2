package io.wisoft.seminar.employee;

import java.util.List;
import java.util.StringJoiner;

public class EmployeeSalary {

  private int sum;
  private float avg;

  public EmployeeSalary() {
  }

  public EmployeeSalary(int sum, float avg) {
    this.sum = sum;
    this.avg = avg;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", EmployeeSalary.class.getSimpleName() + "[", "]")
        .add("sum=" + sum)
        .add("avg=" + avg)
        .toString();
  }
}
