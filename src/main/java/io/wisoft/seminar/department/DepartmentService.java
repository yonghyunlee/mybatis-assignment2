package io.wisoft.seminar.department;

import java.util.List;

public interface DepartmentService {

//  int (final Student department);
//
//  int insertStudentList(final List<Student> studentList);
//
//  int insertStudentListBatch(final List<Student> studentList);

  List<Department> getDepartmentAll();

  List<Department> getDepartmentListByNo(final List<String> studentNoList);

}
