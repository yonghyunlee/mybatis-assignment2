package io.wisoft.seminar.department;

import java.util.StringJoiner;

public class Department {

  private String code;
  private String name;
  private String location;

  public Department() {
  }

  public Department(String code, String name, String location) {
    this.code = code;
    this.name = name;
    this.location = location;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", Department.class.getSimpleName() + "[", "]")
        .add("code='" + code + "'")
        .add("name='" + name + "'")
        .add("location='" + location + "'")
        .toString();
  }
}
